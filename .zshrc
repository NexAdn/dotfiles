# Completion
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
setopt COMPLETE_ALIASES

# Vi input mode
bindkey -v

# Prompt themes
#autoload -Uz promptinit
#promptinit

#prompt adam1

#autoload -Uz oh-my-zsh

#setopt PROMPT_SUBST ; PS1='[%n@%m %c$(__git_ps1 " (%s)")]\$ '

POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="↱"
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="↳"

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon dir dir_writable newline load background_jobs context root_indicator ssh nodeenv vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status history command_execution_time)

source /usr/share/zsh/share/antigen.zsh

antigen use oh-my-zsh
antigen bundle git
antigen bundle colored-man-pages
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle bhilburn/powerlevel9k
antigen theme bhilburn/powerlevel9k
antigen apply

plugins=(archlinux git github mvn node npm sudo systemd yarn)

######################
# Pre shell execution
# end of zsh config
######################

#ssh-add -l &> /dev/null
#if [[ "$?" == 2]]; then
#	test -r ~/.ssh-agent && \
#		eval "$(<~.ssh-agent)" > /dev/null
#
#	ssh-add -l &> /dev/null
#	if [[ "$?" == 2 ]]; then
#		(umask 066; ssh-agent > ~/.ssh-agent)
#		eval "$(<~/.ssh-agent)" > /dev/null
#		$HOME/addssh.sh
#	fi
#fi
#source ~/.bashrc

alias lsl='ls -l'
alias pegro='man'

function startbg {
	nohup bash -c "$@" 2>&1 >> /dev/null &
}

function mkcd() {
	mkdir -p "$@" && cd "$@"
}

function cls() {
	cd "$@" && ls
}

function inclv() {
	sudo lvextend -L+5G syspc01/$1
	sudo resize2fs /dev/syspc01/$1
}

function sleep_until() {
	current_epoch=$(date +%s)
	target_epoch=$(date -d "$1" +%s)

	sleep_seconds=$(( $target_epoch - $current_epoch ))

	echo "Sleeping for $sleep_seconds seconds"

	sleep -- $sleep_seconds
}

function skel() {
	cp -ivR ${HOME}/.local/share/user/skel/${1} ./${2}
}

export HISTFILE=/tmp/adrian_zsh_history

export EDITOR=vim
export VISUAL=$EDITOR

export PATH=$PATH:/home/adrian/cas_linux/ndless/ndless-sdk/toolchain/install/bin:/home/adrian/cas_linux/ndless/ndless-sdk/toolchain/../bin

# dotfiles
config() {
	git --git-dir=$HOME/dotfiles.git --work-tree=$HOME $@
}
