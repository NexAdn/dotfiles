# NexAdn's Dotfiles
Well... Nothing to describe here. Just a bunch of dotfiles I use for my PCs.

# Dependencies
## General functionality
* Window manager: i3-gaps
* Compositor: compton
* Menu: rofi
* Bar: polybar (compiled with mpd support enabled)
* Audio Backend: pulseaudio
* Music Backend: mpd
* Music Client: mpc
* Shell: zsh

## Applications
* Firefox (Web Browser)
* Thunderbird (Mail client)
* Discord (VoIP)
* Rambox (Chat)
